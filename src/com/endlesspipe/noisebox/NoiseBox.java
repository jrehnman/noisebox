package com.endlesspipe.noisebox;

import java.util.EmptyStackException;
import java.util.Random;
import java.util.Stack;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;

public class NoiseBox extends Activity {
	
	public static class OutOfBoundsAssertion extends AssertionError {
		/**
		 * 
		 */
		private static final long serialVersionUID = 7176989005629785291L;
		
		public OutOfBoundsAssertion(Object o) {
			super(o);
		}
	}
	
	public static enum Operation {
		T,
		CONST,
		SIN,
		MUL,
		AND,
		OR,
		SHL,
		SHR;
		
		public static Operation fromInt(int n)
		{
			return Operation.values()[n % Operation.values().length];
		}
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noisebox);
        
        final NumberPicker exprPicker = (NumberPicker) findViewById(R.id.numberPicker1); exprPicker.setMaxValue(Integer.MAX_VALUE);
        final NumberPicker constPicker = (NumberPicker) findViewById(R.id.numberPicker2); constPicker.setMaxValue(Integer.MAX_VALUE);
        final EditText expressionText = (EditText) findViewById(R.id.editText1);
        
        OnValueChangeListener recalculateExpr = new OnValueChangeListener() {
			@Override
			public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
				int exprSeed = exprPicker.getValue();
				int constSeed = constPicker.getValue();
				Random constGen = new Random(constSeed);
				
				Stack<Operation> expression = generateExpression(exprSeed);
				String expressionString = expression2string(expression, constGen);
				expressionText.setText(expressionString);
			}
		};
        
        exprPicker.setOnValueChangedListener(recalculateExpr);
        constPicker.setOnValueChangedListener(recalculateExpr);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_noisebox, menu);
        return true;
    }
    
    public static Stack<Operation> generateExpression(int exprSeed) {
    	Stack<Operation> operationStack = new Stack<Operation>();
    	int reqTerms = 1;
    	
    	do {
    		Operation op = Operation.fromInt(exprSeed & 0x07);
    		exprSeed >>= 3;
    		
    		operationStack.push(op);
    		reqTerms--;
    		
    		reqTerms += operatorArguments(op);
    	} while(reqTerms > 0);
    	
    	return operationStack;
    }
    
    public static int evaluate(Stack<Operation> expression, byte t, int constSeed) {
    	Stack<Byte> valueStack = new Stack<Byte>();
    	Random constGen = new Random(constSeed);
    	
    	do {
    		Operation op = expression.pop();
    		byte value = evaluateOperator(op, t, constGen, valueStack);
    		valueStack.add(value);
    	} while(valueStack.size() > 1);
    	
    	if (valueStack.isEmpty()) {
    		throw new Error(); // TODO: Better error
    	}
    	
    	return valueStack.get(0);
    }
    
    private static byte evaluateOperator(Operation op, byte t, Random constGen, Stack<Byte> valueStack) {
    	byte op1, op2;
    	
    	try {
	    	switch(op) {
	    	case CONST:
	    		return (byte)constGen.nextInt();
	    	case T:
	    		return t;
	    	case SIN:
	    		op1 = valueStack.pop();
	    		return (byte)(255 * Math.sin(op1/255.0));
	    	case MUL:
	    		op1 = valueStack.pop();
	    		op2 = valueStack.pop();
	    		return (byte)(op1 * op2);
	    	case AND:
	    		op1 = valueStack.pop();
	    		op2 = valueStack.pop();
	    		return (byte)(op1 & op2);
	    	case OR:
	    		op1 = valueStack.pop();
	    		op2 = valueStack.pop();
	    		return (byte)(op1 | op2);
	    	case SHL:
	    		op1 = valueStack.pop();
	    		op2 = valueStack.pop();
	    		return (byte)(op1 >> op2);
	    	case SHR:
	    		op1 = valueStack.pop();
	    		op2 = valueStack.pop();
	    		return (byte)(op1 << op2);
	    	}
    	}
    	catch(EmptyStackException e)
    	{
    		throw e; // TODO: Throw specific error
    	}
    	
		// Programmer error, not all enumeration options have been handled
		throw new OutOfBoundsAssertion(op);
	}
    
    private static String expression2string(Stack<Operation> expr, Random constGen) {
    	try {
    		Operation op = expr.remove(0);
	    	switch(op) {
	    	case CONST:
	    		return "" + (constGen.nextInt() % 0xFF);
	    	case T:
	    		return "T";
	    	case SIN:
	    		return "sin(" + expression2string(expr, constGen) + ")";
	    	case MUL:
	    		return expression2string(expr, constGen) + "*" + expression2string(expr, constGen);
	    	case AND:
	    		return expression2string(expr, constGen) + "&" + expression2string(expr, constGen);
	    	case OR:
	    		return expression2string(expr, constGen) + "|" + expression2string(expr, constGen);
	    	case SHL:
	    		return expression2string(expr, constGen) + ">>" + expression2string(expr, constGen);
	    	case SHR:
	    		return expression2string(expr, constGen) + "<<" + expression2string(expr, constGen);
	    	}
	    	
	    	// Programmer error, not all enumeration options have been handled
			throw new OutOfBoundsAssertion(op);
    	}
    	catch(EmptyStackException e)
    	{
    		throw e; // TODO: Throw specific error
    	}
	}

	public static int operatorArguments(Operation op) {
    	switch(op) {
    	case CONST:
    	case T:
    		return 0;
    	case SIN:
    		return 1;
    	case MUL:
    	case AND:
    	case OR:
    	case SHL:
    	case SHR:
    		return 2;
    	}
    	
		// Programmer error, not all enumeration options have been handled
		throw new OutOfBoundsAssertion(op);
    }
}
